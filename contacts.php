<?php
require_once('connector.php');

$sql = "SELECT * FROM contacts ORDER BY firstname, lastname";
$query = mysqli_query($conn, $sql);
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Phonebook</title>
	</head>
	<body>
		<a href="contactform.php">Add Contact</a><br>
		<table border="1">
			<thead>
				<th>ID</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Phone Number</th>
				<th colspan="2">Actions</th>
			</thead>
			<tbody>
				<?php while ($row=mysqli_fetch_assoc($query)) { ?>
					<tr>
						<td><?php echo $row['contact_id'] ?></td>
						<td><?php echo $row['firstname'] ?></td>
						<td><?php echo $row['lastname'] ?></td>
						<td><?php echo $row['phone'] ?></td>
						<td><a href="editcontact.php?contact_id=<?php echo $row['contact_id'] ?>">Edit</a></td>
						<td><a href="deletecontact.php?contact_id=<?php echo $row['contact_id'] ?>">Delete</a></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</body>
</html>