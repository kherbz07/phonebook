<?php
require_once('connector.php');
$contact_id = $_GET['contact_id'];
$sql = "SELECT * FROM contacts WHERE contact_id=".$contact_id;
$query = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($query);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Edit Contact</title>
</head>
<body>
	<form action="edit.php" method="POST">
		<input type="hidden" name="contact_id" value="<?php echo $row['contact_id']?>">
		<label>First Name: </label>
		<input type="text" name="firstname" value="<?php echo $row['firstname']?>">
		<br>
		<label>Last Name: </label>
		<input type="text" name="lastname" value="<?php echo $row['lastname']?>">
		<br>
		<label>Phone Number: </label>
		<input type="text" name="phone" value="<?php echo $row['phone']?>">
		<br>
		<input type="submit" value="Save Changes">
	</form>
</body>
</html>